/*	Author:
		TMW - Yemi Kehinde
*/

// Create a closure to maintain scope of the '$' and KO (Kickoff)
;(function(KO, $) {

	$(function() {
		// Any globals go here in CAPS (but avoid if possible)

		// follow a singleton pattern
		// (http://addyosmani.com/resources/essentialjsdesignpatterns/book/#singletonpatternjavascript)

		KO.Config.init();

		$('.image-swap').attr('src', 'http://gps.tmw.co.uk/ajax/profile_pic.php?email=notselected@tmwunlimited.com&size=large1');

		$('.choice-form-input').on('change', function(){
			$(this).parent().find('.image-swap').attr('src', 'http://gps.tmw.co.uk/ajax/profile_pic.php?email=' + $(this).find('option:selected').val() +'&size=large1');
		});

	});// END DOC READY

	
		
	


	KO.Config = {
		variableX : '', // please don't keep me - only for example syntax!

		init : function () {
			console.debug('Kickoff is running');
		}
	};

	// Example module
	/*
	KO.MyExampleModule = {
		init : function () {
			KO.MyExampleModule.setupEvents();
		},

		setupEvents : function () {
			//do some more stuff in here
		}
	};
	*/

})(window.KO = window.KO || {}, jQuery);